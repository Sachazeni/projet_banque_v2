package fr.afpa.entite;


import java.util.ArrayList;


public class Banque {
	
	private String nom;

	private ArrayList<Agence> listeAgence;

	//Constructeur par d�faut
	public Banque() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	//Constructeur avec l'instantiation d'une liste d'agence 
	public Banque(String nom) {
		super();
		this.nom = nom;
		this.listeAgence=new ArrayList<Agence>();
	}

	//Getter et Setter
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	


	public ArrayList<Agence> getListeAgence() {
		return listeAgence;
	}

	public void setListeAgence(ArrayList<Agence> listeAgence) {
		this.listeAgence = listeAgence;
	}

	@Override
	public String toString() {
		return "Banque [nom=" + nom + ", listeAgence=" + listeAgence + "]";
	}
	
}
