package fr.afpa.entite;

import java.time.LocalDate;
import java.util.ArrayList;


public class Client {
	private String nom;
	private String prenom;
	private String id;
	private LocalDate dateNaissance;
	private String email;
	private String login;
	private boolean status;
	private String mdp;
	ArrayList<Compte> listeCompte;
	
	private static int idQuiSincremente;


	public Client(String nom, String prenom, LocalDate dateNaissance, String login, String email, String mdp) {
		
		this.nom = nom;
		this.prenom = prenom;
		this.id = ""+nom.toUpperCase().charAt(0)+prenom.toUpperCase().charAt(0)+String.format("%06d", ++idQuiSincremente);
		this.login=login;
		this.dateNaissance = dateNaissance;
		this.email = email;
		this.status=false;
		this.mdp=mdp;
		this.listeCompte = new ArrayList<Compte>();
	}


//les gets et les sets
	public boolean isStatus() {
		return status;
	}



	public void setStatus(boolean status) {
		this.status = status;
	}



	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}



	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDate getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(LocalDate dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public ArrayList<Compte> getListeCompte() {
		return listeCompte;
	}

	public void setListeCompte(ArrayList<Compte> listeCompte) {
		this.listeCompte = listeCompte;
	}

	public String getMdp() {
		return mdp;
	}


	public void setMdp(String mdp) {
		this.mdp = mdp;
	}


	public Client() {
		
	}

	@Override
	public String toString() {
		return  "Numero client : "+id+
				"\nLogin : "+login+
				"\nNom : " + nom + 				
				"\nPrenom : " + prenom + 
				"\nDate de Naissance : " + dateNaissance+
				"\n Mail :" + email;
	}
}
