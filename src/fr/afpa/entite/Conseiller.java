package fr.afpa.entite;


import java.util.ArrayList;


public class Conseiller {
	 
	private String nom;
	private String prenom;
	private String email;
	private String identifiantConseiller;
	private String mdp;
	private String loginDeConnexion;
	private ArrayList<Client> listeClient;
	private static int idIncremente;
	
	/**
	 * Constructeur par default du Conseiller
	 */
	public Conseiller() {
	}
	
	public Conseiller(String nom, String prenom, String login, String email, String mdp) {
	
		this.nom = nom;
		this.prenom = prenom;
		this.loginDeConnexion=login;
		this.identifiantConseiller=""+nom.toUpperCase().charAt(0)+String.format("%05d",++idIncremente);
		this.email = email;
		this.mdp=mdp;

		this.listeClient = new ArrayList<Client>();
	}


	
	//les Gets et les Sets
	public ArrayList<Client> getListeClient() {
		return listeClient;
	}
		
	public void setListeClient(ArrayList<Client> listeClient) {
		this.listeClient = listeClient;
	}
	
	
	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getIdentifiantConseiller() {
		return identifiantConseiller;
	}

	public void setIdentifiantConseiller(String identifiantConseiller) {
		this.identifiantConseiller = identifiantConseiller;
	}
	
	public String getLoginDeConnexion() {
		return loginDeConnexion;
	}

	public void setLoginDeConnexion(String loginDeConnexion) {
		this.loginDeConnexion = loginDeConnexion;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Conseiller : "+identifiantConseiller
				+ nom + " " + prenom + 
				"\nEmail du conseiller : "+email
				+ "\nListe de Clients gerées par le conseiller : " + listeClient;
	}


	
}
