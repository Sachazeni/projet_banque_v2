package fr.afpa.entite;


public class Compte {
	private String numeroCompteB;
	private double solde;
	private boolean decouvert;
	private double frais;
	boolean compteActif;
	public Compte() {

	}

	public Compte(String numeroCompteB, double solde, boolean decouvert, boolean compteActif) {

		this.numeroCompteB = numeroCompteB;
		this.solde = solde;
		this.decouvert = decouvert;
		this.compteActif = compteActif;
		this.frais=25d;
	}


	// Les gets et les Sets
	public String getNumeroCompteB() {
		return numeroCompteB;
	}

	public void setNumeroCompteB(String numeroCompteB) {
		this.numeroCompteB = numeroCompteB;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(Double solde) {
		this.solde = solde;
	}

	public boolean isDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	
	public double getFrais() {
		return frais;
	}

	public void setFrais(double frais) {
		this.frais = frais;
	}

	public boolean isCompteActif() {
		return compteActif;
	}

	public void setCompteActif(boolean compteActif) {
		this.compteActif = compteActif;
	}

	@Override
	public String toString() {
		return "Compte Courant : "
				+"\nNumero : " + numeroCompteB 
				+ "\nSolde : " + solde
				+ "\n Decouvert autorisé : " + decouvert
				+"\nCompte actif : "+compteActif
				+ "\nFrais du compte : " + frais;
	}


}
