package fr.afpa.entite;

public class LivretA extends Compte {
	
	public LivretA() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LivretA(String numeroCompteB, double solde, boolean decouvert, boolean compteActif) {
		
		super(numeroCompteB, solde, decouvert, compteActif);

	}


	@Override
	public String toString() {
	
		return "Livret A :"+"\nNumero : "+ super.getNumeroCompteB()
				+"\nSolde : "+super.getSolde()
				+"\nDecouvert autoris� : "+super.isDecouvert()
				+"\nCompte actif : "+super.isCompteActif()
				+"\nFrais du compte : "+ super.getFrais();
	}


}
