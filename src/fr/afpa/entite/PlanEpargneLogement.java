package fr.afpa.entite;

public class PlanEpargneLogement extends Compte {
	private double frais;

	public PlanEpargneLogement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PlanEpargneLogement(String numeroCompteB, double solde, boolean decouvert, boolean compteActif) {
		super(numeroCompteB, solde, decouvert, compteActif);
	
	}

	
	
	public double getFrais() {
		return frais;
	}

	public void setFrais(double frais) {
		this.frais = frais;
	}

	@Override
	public String toString() {
		return "Plan Epargne Logement : "
				+"\nSolde : "+super.getSolde()
				+"\nDecouvert autoris� : "+super.isDecouvert()
				+"\nCompte actif : "+super.isCompteActif()
				+"\nFrais du compte : "+ frais;
	}
}
