package fr.afpa.entite;

import java.util.ArrayList;

public class Agence {
	private String codeAgence;
	private String nomAgence;
	private String numeroRue;
	private String rue;
	private String ville;
	private String codePostal;
	private ArrayList<Conseiller> listeConseiller;
	
	//Constructeur par d�faut
	public Agence() {
		super();
		// TODO Auto-generated constructor stub
	}

	//Constructeur avec instantiation d'une liste de client
	public Agence(String codeAgence, String nomAgence, String numeroRue, String rue, String ville, String codePostal) {
		super();
		this.codeAgence = codeAgence;
		this.nomAgence = nomAgence;
		this.numeroRue = numeroRue;
		this.rue = rue;
		this.ville=ville;
		this.codePostal = codePostal;

		this.setListeConseiller(new ArrayList<Conseiller>());
	}

	//Getter et Setter
	

	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}
	
	public String getNumeroRue() {
		return numeroRue;
	}

	public void setNumeroRue(String numeroRue) {
		this.numeroRue = numeroRue;
	}
	public String getNomAgence() {
		return nomAgence;
	}

	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}
	
	

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public ArrayList<Conseiller> getListeConseiller() {
		return listeConseiller;
	}

	public void setListeConseiller(ArrayList<Conseiller> listeConseiller) {
		this.listeConseiller = listeConseiller;
	}


	
	@Override
	public String toString() {
		return "Agence n°" + codeAgence + "\nNom de l'agence : " + nomAgence + "\nAdresse " + numeroRue + " rue "
				+ rue +"\nCode postal : " + codePostal+" "+ville;
	}

	

}
