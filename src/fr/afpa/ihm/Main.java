package fr.afpa.ihm;

import java.time.LocalDate;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;


public class Main{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		Banque banquePicsou = new Banque("LA BANQUE A PICSOU");
		banquePicsou.getListeAgence().add(new Agence("547", "CDA", "20", "Luxembourg", "Roubaix", "59650"));
		banquePicsou.getListeAgence().get(0).getListeConseiller().add(new Conseiller("Benjira", "Mohammed", "CO9876", "email@mail.fr", "java"));
		banquePicsou.getListeAgence().get(0).getListeConseiller().get(0).getListeClient().add(new Client("Dziamski", "Nicolas", LocalDate.of(1989, 03,17), "BD123456", "email@mail.fr", "0000"));
		banquePicsou.getListeAgence().get(0).getListeConseiller().get(0).getListeClient().add(new Client("Zenina", "Alexandra", LocalDate.of(1989, 03,17), "BD123476", "email@mail.fr", "0001"));
		while(true) {
		AffichageMenu.MenuConnexion(banquePicsou);
		}
	
	
	}
	
	
	

	}

