package fr.afpa.ihm;

import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;

public class AffichageComptes {
	/**
	 * Methode pour afficher les 3 comptes actif d'un client
	 * @param client -Le Client 
	 */
	public static void afficherComptes(Client client) {
	for (Compte compte : client.getListeCompte()) {
		if(compte.isCompteActif()&&compte!=null) {
			System.out.println(compte);
		}
		
	}
	
	}
}
