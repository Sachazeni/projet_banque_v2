package fr.afpa.ihm;



import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

import fr.afpa.entite.Conseiller;

public class AffichageBanque {
	public static void afficherToutesAgenceBanque(Banque banque) {
		for (Agence ag : banque.getListeAgence()) {
			System.out.println(ag+"\n__________________________");
		}
	}

	/**
	 * Methode pour afficher tous les conseillers d'une agence
	 * selon l'affichage defini.
	 * @param agence
	 */
	public static void afficherConseillers(Agence agence) {
		for (Conseiller conseiller : agence.getListeConseiller()) {
			System.out.println(conseiller+"\n___________________________");

		}
	}



}
