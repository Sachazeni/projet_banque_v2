package fr.afpa.ihm;
import java.util.Scanner;
import fr.afpa.controle.ControleAuthentifications;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Banque;

public class AffichageMenu {
	/**
	 * Menu affiche lors de la permiere connexion
	 * @param banque
	 */
	public static void MenuConnexion(Banque banque) {
		
		
		Scanner in=new Scanner(System.in);
		int choix=0;
		boolean flag=false;
		
		do{
		System.out.println("Connexion en tant que : \n1-Administrateur \n2-Conseiller \n3-Client");
		
		choix=Integer.parseInt(ControleSaisie.saisieChiffres(in, 1));
		if(choix==1||choix==2||choix==3) {
			flag=true;
		}
		}while(!flag);
		if(choix==1) {
		ControleAuthentifications.authentificationAdmin(banque); 
		}
		else if(choix==2) {
			
		ControleAuthentifications.authentificationConseiller(banque);
		}
		else if(choix ==3) {
		ControleAuthentifications.authentificationClient(banque);
		}

	}
	/**
	 * Menu affiché à l'administratteur apres sa connexion
	 * @param banque
	 */
	public static void menuApresConnexionAdmin() {
		System.out.println();
		System.out.println("\t     Bienvenue ");
		System.out.println("********************************");
		System.out.println("\t      MENU");
		System.out.println();
		System.out.println("1- Créer une agence");
		System.out.println("2- Afficher toutes les agences de la banque");
		System.out.println("3- Créer un conseiller");
		System.out.println("4- Afficher des conseillers d'une Agence spécifique");
		System.out.println("5- Gestion de clients");//erreurs
		System.out.println("6- Désactiver un client specifique");
		System.out.println("7- Désactiver un compte specifique");// pas fait
		System.out.println();
		System.out.println("8- Quitter");
		System.out.println("**********************************");
		System.out.println("Votre choix:");
		System.out.println("**********************************");
	
	}
	public static void menuChoixGestionClient() {
		System.out.println("********************************");
		System.out.println("\t      MENU");
		System.out.println("********************************");
		System.out.println("0- Créer nouveau client");
		System.out.println("1- Afficher les informations d'un client");
		System.out.println("2- Modifier les informations d'un client");
		System.out.println("3- Changer la domicilation d'un client ");//pas fait encore
		System.out.println("4- Gestion des comptes");
		System.out.println();
		System.out.println("5- Quitter");
		System.out.println();
		System.out.println("Votre choix:");

	}
	
	public static void menuModifClient() {
		System.out.println("********************************");
		System.out.println("      Modifications client");
		System.out.println("********************************");
		System.out.println("1- Nom");
		System.out.println("2- Prenom");
		System.out.println("3- Adresse mail");
		System.out.println("4- Date de naissance");
	
		System.out.println("5- Quitter");
		System.out.println();
		System.out.println("Quelles informations souhaitez vous modifier");
	}
	
	public static void menuGestionComptes() {
		System.out.println("********************************");
		System.out.println("      Gestion des comptes");
		System.out.println("********************************");
		System.out.println("0- Creation compte");
		System.out.println("1- Historique");
		System.out.println("2- Imprimer");
		System.out.println("3- Retirer");
		System.out.println("4- Deposer");
		System.out.println("5- Virement");
		System.out.println("6- Quitter");
		System.out.println();
		System.out.println("Que souhaitez vous faire?");
		
		
	}
	
	public static void menuSpecialClient(){
		System.out.println("1- Afficher mes informations");
		System.out.println("2- Afficher mes comptes");
		System.out.println("3- Faire un Depot");
		System.out.println("4- Faire un retrait");
		System.out.println("5- Faire un virement");
		System.out.println("6-quitter");
	}
}
