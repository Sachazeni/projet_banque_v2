package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.services.ServiceBanque;

public class AffichageClient {

	
	/**
	 * Methode qui va gerer l'affichage des infos du client
	 * @param client
	 */
	public static void afficherInfosClient(Banque banque) {
		String repUtilis="";
		ServiceBanque servBank=new ServiceBanque();
		Client client;
		Scanner scanner=new Scanner(System.in);

		System.out.println("Entrez l'identifiant client");

		repUtilis=ControleSaisie.saisieIdentifiant(scanner);

		client=servBank.rechercherIdentifiantClient(repUtilis, banque);
		if(client!=null) {
			System.out.println(client);
			for (Compte cmt : client.getListeCompte()) {
				System.out.println(cmt);

			}

		}
	}
}
