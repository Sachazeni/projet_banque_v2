package fr.afpa.ihm;

import fr.afpa.entite.Client;

public class AffichageConseiller {
	
	/**
	 * Methode qui affiche le menu apres la conexion du Conseiller
	 */
	public static void menuConseiller() {
		
		System.out.println("					   	Bienvenue � la Banque � Picsou");
		System.out.println();
		System.out.println("					   	MENU");
		System.out.println();
		System.out.println("1- Mes Informations");
		System.out.println("2- Creer un nouveau client");
		System.out.println("3- Gestion d'un client existant");
		System.out.println("4- Changer la domiciliation d'un client");
		System.out.println("5-");
		System.out.println();
		System.out.println("6- Quitter");
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("Votre choix:");
	}
	
/**
 * Menu de gestion d'un client dont l'identifiant fut demand� au prealable	
 * @param client sur le quel l'utilisateur souhaite travailler.
 */
public static void menuConseillerGestionClient(Client client) {
	     System.out.println("1-Afficher les informations du client"
	    		 
						+ "\n2-Modifier les informations du client"
				
						+ "\n3-Consulter les comptes du client"
						
						+ "\n4-Cr�er un nouveau compte pour le client"
						
						+ "\n5-Faire un virement � la demande du client"
						
						+ "\n6-Imprimer le relev� d'un compte bancaire du client"
						
						+ "\n7-Alimenter le compte du client"
						
						+ "\n8-Realiser un retrait � la demande du client");
	    
	     System.out.println();
	     
	     System.out.println("Votre choix :");
		
	
	
}

/**
 * Affiche le menu pour la modifications des information du client
 * @param client sur le quel les modifications seront effectu�s
 */
public static void affichreModifInfosClient(Client client) {
	
	System.out.println("Quelle modifications voulez vous apporter au client?"
					+ "\n1-Modifier le nom du client"
					+ "\n2-Modifier le pr�nom du client"
					+ "\n3- Modifier l'adresse mail du client"
					+ "\n4-Modifier la date de naissance"
					+ "\n"
					+ "\n5-Quitter");

}





}
