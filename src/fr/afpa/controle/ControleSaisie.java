package fr.afpa.controle;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class ControleSaisie {

	public static String saisieLettre(Scanner in) {
		String chaine;
		chaine = in.nextLine();
		while (!ControleVerif.estLettre(chaine)) {
			System.out.println("Saisie invalide (Seuls les caractères alphabetiques sont autorisés)");
			chaine = in.nextLine();
		}
		return chaine;
	}

	public static LocalDate saisieDate(Scanner in) {
		String chaine;
		chaine = in.nextLine();
		while (!ControleVerif.estDate(chaine)) {
			System.out.println("Entrer une date valide (dd//MM//yyyy)");
			chaine = in.nextLine();
		}
		LocalDate date = LocalDate.parse(chaine, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		return date;
	}

	public static String saisieChiffres(Scanner in, int nb) {
		String numero = in.nextLine();
		while (!(ControleVerif.estChiffre(numero) && (nb == -1 || numero.length() == nb))) {
			System.out.println("Entrer uniquement "+ nb+ " chiffres");
			numero = in.nextLine();
		}
		return numero;

	}

	public static String saisieEmail(Scanner in) {
		String email;
		email = in.nextLine();
		while (!ControleVerif.estMail(email)) {
			System.out.println(
					"Entrer un mail valide");
			email = in.nextLine();
		}
		return email;
	}

	public static String saisieMajuscules(Scanner in, int nb) {
		String code = in.nextLine();
		while (!(ControleVerif.estMajuscule(code) && code.length() == nb)) {
			System.out.println("Entrer " + nb + " Majuscules");
			code = in.nextLine();
		}
		return code;
	}

	public static String saisieRue(Scanner in) {
		String rue = in.nextLine();
		while (rue.length() == 0) {
			System.out.println("Entrer un nom rue nom vide");
			rue = in.nextLine();
		}
		return rue;
	}
	
	public static String saisieIdentifiant(Scanner in) {
		String identifiantClient=in.nextLine();
		while(!(ControleClient.siIdentifiantValide(identifiantClient))) {
			System.out.println("L'identifiant client est composé uniquement de deux lettres majuscules suivis de six chiffres");
			identifiantClient=in.nextLine();
		}
		return identifiantClient;
	}
	/**
	 * Methode pour verifier si le login est ecrit correctement
	 * @param scanner
	 */
	public static String siLoginConseillerCorrect(Scanner scanner) {
		String loginEntree=scanner.nextLine();
		while(!loginEntree.matches("[C][O]\\d{4}")) {
			System.out.println("Login incorrect veuillez entrer un autre login");
			loginEntree=scanner.nextLine();
			
		}
		return loginEntree;
	}

}
