package fr.afpa.controle;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.services.ServiceBanque;

public class ControleVerif {
	/**
	 * Compare la saisie au regex
	 * @param chaine
	 * @return boolean true si correspond faux si ne correspond pas
	 */
	public static boolean estLettre(String motAVerifier) {

		String regex = "[A-Za-z]+";
		return motAVerifier.matches(regex);
	}

	public static boolean estChiffre(String motAverifier) {
		String regex="\\d+";
		return motAverifier.matches(regex);
	}

	/**
	 * Methode pour verifier si la date est valide
	 * @param dateAverifier
	 * @return si date est valide alors renvoyer true sinon false
	 */
	public static boolean estDate(String dateAverifier) {
		try {
			LocalDate date = LocalDate.parse(dateAverifier, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		} 
		catch (Exception e) {
			return false;
		}
		return true;

	}
	
	/**
	 * Methode pour verifier si le mot est compose de majuscules
	 * @param motAVerifier
	 * @return boolean true ou false si le mot est compos�e de majuscule et false dans le cas contraire
	 */
	public static boolean estMajuscule(String motAVerifier) {
		String regex = "[A-Z]+";
		return motAVerifier.matches(regex);	
	}
	
	/**
	 * Methode pour controler si le mail entr�e en parametre est correct
	 * @param mailAverifier
	 * @return boolean true si le mail est correct, false dans le cas contraire
	 */
	public static boolean estMail(String mailAverifier) {
		String regex="\\w+(\\.?\\-*\\w+)*@([a-zA-Z0-9]{3,13})\\.[a-zA-Z]{2,3}";
		return mailAverifier.matches(regex);
	}
 
	/**
	 * Methode qui va parcourir la liste des clients si le login correspond � un client
	 * @param identifiant generee aleatoirement par le programme
	 * @param banque
	 * @return true si l'identifiant est disponible, false dans le cas contraire
	 */
	public static boolean siLoginClientDisponible(String login, Banque banque) {
		ServiceBanque bank=new ServiceBanque();
		if(bank.rechercherClient(login, banque)==null) {
			return  true;
		}
		else return false;
	}

	/**
	 * Methode pour verifier si le code de l'agence est déjà attribué ou pas
	 * @param code qu'on souhaite verifier
	 * @param banque dans la quelle se trouvent les agences à parcourir
	 * @return boolean true si le codeAgence est disponible, false dans le cas contraire
	 */
	public static boolean siCodeAgenceDisponible(String code, Banque banque) {
		
		ServiceBanque bk= new ServiceBanque();
		if(bk.rechercherAgence(code, banque)==null) {
			return true;
		}
		else
			System.out.println("Code agence déjà attribue");
		return false;
	}
	/**
	 * Methode pour verifier si la banque possede des agences
	 * @param banque
	 * @return true si la banque a des agence; false dans le cas contraire
	 */
	public static boolean siBanquePossedeDesAgences(Banque banque){
	if (banque.getListeAgence().size() == 0) {
		System.out.println("La banque ne possede aucune Agence veuillez créer une agence avant toute autre manipulation");
		return false;
	}
	else {
		return true;
	}
	}
	/**
	 * Methode pour verifier si la liste des conseillers contient au moins 
	 * un conseiller.
	 * @param agence ou on cherche le conseiller
	 * @return true si l'agence contient au moins un conseiller, revoit false 
	 * dans le cas contraire
	 */
	public static boolean siAgenceContientConseiller(Agence agence) {
		if(agence.getListeConseiller().size()==0) {
		System.out.println("L'Agence ne possede aucun Conseiller veuillez créer un conseiller avant toute autre manipulation");
			return false;
		}
		
		else {
			return true;
		}
	}
	
	
	/**
	 * Methode pour generer un numero de compte aleatoire
	 * @return
	 */
	public static String GenererNumeroAleatoire(int nombreSouhaite) {
		String numCompte="";
		Random chiffreAlea=new Random();
		int i=nombreSouhaite;
		while(i>0){
			numCompte=numCompte+chiffreAlea.nextInt(10);
			i--;	
		}
		return numCompte;

	}
	/** 
	 * Methode qui parcourt tous les logins des conseillers pour 
	 * verifier si celui la est disponible
	 * @param login donnée par l'utilisateur
	 * @param banque dans la quelle le conseiller se trouve
	 * @return un boolean true si disponible, false dans le cas contraire.
	 */
	public static boolean siLoginConseillerExisteDeja(String login,Banque banque) {
		ServiceBanque sb=new ServiceBanque();
		if(sb.rechercherConseillerViaLogin(login, banque)==null) {
			return true;
		}
		else { 
			System.out.println("Ce login existe déjà!");
			return false;
		}
	}
	
}


