package fr.afpa.controle;

public class ControleClient {
	/**
	 * Methode qui verifie si l'identifiant entrée est correct
	 * @param identifiantAverifier
	 * @return un boolean true si correct ou un boolean false dans le cas contraire
	 */
	public static boolean siIdentifiantValide(String identifiantAverifier) {
		String regex="[A-Z]{2}\\d{6}";
		return(identifiantAverifier.matches(regex));
				
	}
	/**
	 * Methode qui verifie si le login entrée est composée de 10chiffres
	 * @param login
	 * @return boolean true si correct false dans le cas contraire
	 */
	public static boolean siLoginValide(String login) {
		String regex="\\d{10}";
		return(login.matches(regex));
	}
	
	
	
	
	
	
}
