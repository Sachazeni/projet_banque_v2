package fr.afpa.controle;

import java.util.Scanner;

import fr.afpa.entite.Client;

public class ControleComptes {


	/**
	 * Methode pour verifier si le client possede le nombre max de comptes ou pas
	 * @param client
	 * @return true si le client est a son max, false dans le cas contraire
	 */
	public static boolean siPossedeTroisComptes(Client client) {
		if(client.getListeCompte().size()==3) {
			System.out.println("Ce client a déja trois comptes");
			return true;}
		else {
			return false;
		}
	}

	/**
	 * Controle que le montant saisie ne soit pas negatif
	 * @param montant
	 * @return vrai si le montant entrée est positif false dans le cas contraire
	 */
	public static double isPositif () {	
		Scanner in= new Scanner(System.in);
		double montant=0;
		while(montant<0) {
			montant=in.nextDouble();
			if(montant>0) {
				return montant;
			}
			System.out.println("Le montant ne peut etre negatif");

		}
		return montant;
	}
}
