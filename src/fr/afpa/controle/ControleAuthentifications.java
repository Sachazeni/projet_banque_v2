package fr.afpa.controle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.AffichageBanque;
import fr.afpa.services.ServiceBanque;
import fr.afpa.services.ServiceClient;
import fr.afpa.services.ServiceConseiller;

public class ControleAuthentifications {


	/**
	 * Methode qui gere l'authentification du login en fonction du profil
	 * si le login entree est faux: redemande le login
	 */
	public static void authentificationAdmin(Banque banque) {

		String profil="Administrateur";
		String regex="[A][D][M]\\d{2}";
		String chemin="..\\projet_banque_v2\\Ressources\\Admin\\Login.txt";

		Scanner in=new Scanner(System.in);
		boolean verifLog=false;

		while(!verifLog) {
			System.out.println("Connexion en tant que "+profil+"\nEntrez votre login: ");
			String loginEntree = in.nextLine();
			String verificationLogin=null;


			if(loginEntree.matches(regex)) {
				try {
					FileReader fr = new FileReader(chemin);
					BufferedReader br = new BufferedReader(fr);
					while(br.ready()) {
						verificationLogin=br.readLine();
					}
					br.close();
				}catch(Exception e) {
					System.out.println("Erreur"+e);
				}
				if(loginEntree.equals(verificationLogin)) {
					passwordAdm(banque);
					verifLog=true;
					break;
				}
			}else {
				System.out.println("Mauvaise Saisie");
				verifLog=false;
			}

		}
	}


	/**
	 * Methode qui demande et verifie le mot de passe de en fonction du profil
	 * si le mot de passe entree est incorrect redemnade le mot de passe
	 */
	public static void passwordAdm(Banque banque) {
		ServiceBanque bank=new ServiceBanque();
		String chemin="..\\projet_banque_v2\\Ressources\\Admin\\Password.txt";

		Scanner scanner = new Scanner(System.in);
		
		boolean siMotDePasseCorrect=false;
		String mdpContenuFichier=null;		
		do{
			System.out.println("Veuillez entrer le mot de passe: ");
			String entreeUtilisateur=scanner.nextLine();
			try {
				FileReader fr = new FileReader(chemin);
				BufferedReader bufReader = new BufferedReader(fr);
				while(bufReader.ready()) {
					mdpContenuFichier=bufReader.readLine();
				}
				bufReader.close();
			}
			catch(Exception e) {
				System.out.println("Erreur"+e);
			}
			if(entreeUtilisateur.equals(mdpContenuFichier)) {
				siMotDePasseCorrect=true;		}
		}while(!siMotDePasseCorrect);

		bank.choixAdmin(banque);

	}
	
public static void authentificationConseiller(Banque banque) {
Scanner sc=new Scanner(System.in);
ServiceBanque servB=new ServiceBanque();
ServiceConseiller servCons=new ServiceConseiller();
Conseiller conseiller;
	System.out.println("Connexion en tant que Conseiller, entrez le login:");
	String loginEmp=ControleSaisie.siLoginConseillerCorrect(sc);
	conseiller=servB.rechercherConseillerViaLogin(loginEmp, banque);
	if(conseiller!=null) {
		servCons.gestionClientParConseiller(banque, conseiller);
	}
	
}
public static void authentificationClient(Banque banque) {
	Scanner sc=new Scanner(System.in);
	
	ServiceBanque servB=new ServiceBanque();
	ServiceClient servC= new ServiceClient();
	Client client;
	
		
		System.out.println("Connexion en tant que Client, entrez le login:");
		String loginClient=sc.nextLine();
		if(ControleClient.siIdentifiantValide(loginClient)) {
			client=servB.rechercherClient(loginClient, banque);
			if(client!=null) {
				servC.menuSpecialClient(client);
		}
		
		
		}
	
	
}
	



}


