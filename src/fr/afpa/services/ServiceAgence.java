package fr.afpa.services;


import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.controle.ControleVerif;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;


public class ServiceAgence {

	/**
	 * Methode pour creer une agence dans la banque
	 * @banque -Banque dans la liste de laquelle l'agence sera ajout�e 
	 */
	public Agence creerAgence(Banque banque) {
		Scanner in =new Scanner(System.in);
		String code="";
		String nom="";
		String num="";
		String rue="";
		String cP="";
		String ville="";
		//Demande de saisie des informations et appel de des fonctions de contr�le de saisie
		do{
		code = ControleVerif.GenererNumeroAleatoire(3);
		}while(!ControleVerif.siCodeAgenceDisponible(code, banque));
			
		System.out.println("Entrer nom de l'Agence: ");
		nom = ControleSaisie.saisieLettre(in);

		System.out.println("Entrer numero de rue de l'agence: ");
		num = in.nextLine();

		System.out.println("Entrer le nom de la rue: ");
		rue = in.nextLine();

		System.out.println("Entrer le code postal: ");
		cP = ControleSaisie.saisieChiffres(in, 5);
		
		System.out.println("Entrez la ville");
		ville=in.nextLine();
		
		Agence agence=new Agence(code, nom, num, rue, cP,ville);
		
		return agence;
	}

	/**
	 * Methode pour rechercher le conseiller dans la liste des agences
	 * @param id - identifiant specifique au conseiller
	 * @param agence - Agence dans la quelle on va chercher le conseiller
	 * @return Conseiller
	 */
	public  Conseiller rechercherConseiller(String id, Agence agence) {

		for(Conseiller cons : agence.getListeConseiller()) {
			if(cons !=null && id.equals(cons.getIdentifiantConseiller()) ) {

				return cons;

			}
		}
		return null;
	}
	
	/**
	 * Methode pour rechercher le conseiller dans la liste des agences
	 * @param login - login specifique au conseiller
	 * @param agence - Agence dans la quelle on va chercher le conseiller
	 * @return Conseiller
	 */
	public  Conseiller rechercherConseillerViaLogin(String login, Agence agence) {

		for(Conseiller cons : agence.getListeConseiller()) {
			if(cons !=null && login.equals(cons.getLoginDeConnexion()) ) {

				return cons;

			}
		}
		return null;
	}
	/**
	 * Methode pour rechercher un client via le login 
	 * @param login du client
	 * @param agence dans la quelle se trouve le conseiller
	 * @return Client.
	 */
	public Client rechercherClient(String login, Agence agence) {
		Client client;
		ServiceConseiller serviceConseil=new ServiceConseiller();

		for(Conseiller conseiller: agence.getListeConseiller()) {
			client=serviceConseil.rechercherClient(login, conseiller);
			if (client!=null) {
				return client;
			}
		}

		return null;
	}
	/**
	 * Methode pour rechercher un client via son identifiant
	 * @param identifiant
	 * @param conseiller
	 * @return
	 */
	public Client rechercherIdentifiantClient(String identifiant, Agence agence) {
		Client client; 
		ServiceConseiller serviceConseil=new ServiceConseiller();
		for (Conseiller conseiller : agence.getListeConseiller()){
			client=serviceConseil.rechercherIdentifiantClient(identifiant, conseiller);
			if(client!=null){
				return client;
			}

		}
		return null;
	}
	/**Methode pour rechercher le Client via son nom
	 * @param nom du Client recherch�
	 * @param agence dans la quelle se trouve le client
	 * @return client si  client trouv� sinon null;
	 */
	public Client rechercherNomClient(String nom, Agence agence) {
		Client client;
		ServiceConseiller serviceConseil=new ServiceConseiller();
		for (Conseiller conseiller : agence.getListeConseiller()) {
			client=serviceConseil.rechercherNomClient(nom, conseiller);
			if(client!=null) {
				return client;
			}

		}
		return null;
	}

	/**
	 * Rechercher le client via son numero de compte Bancaire
	 * @param numeroCB 
	 * @param agence dans la quelle se trouve le client
	 * @return le client trouv�
	 */
	public Client rechercherClientViaCompte(String numeroCB, Agence agence) {
		Client clientRecherche;
		ServiceConseiller serviceConseil=new ServiceConseiller();

		for(Conseiller conseiller: agence.getListeConseiller()) {
			clientRecherche=serviceConseil.rechercherClientViaCompte(numeroCB, conseiller);
			if(clientRecherche!=null) {
				return clientRecherche;
			}

		}
		return null;
	}

	/**
	 * Methode pour rechercher un compte Bancaire via Agence
	 * @param numeroCompte le compte recherch�
	 * @param agence
	 * @return CompteCourant recherch�.
	 */
	public Compte rechercherCompteB(String numeroCompte, Agence agence) {
		Compte compte;
		ServiceConseiller scn=new ServiceConseiller();

		for (Conseiller conseiller : agence.getListeConseiller()) {
			compte=scn.rechercherCompteB(numeroCompte, conseiller);
			if(compte!=null) {
				return compte;

			}

		}
		return null;
	}
}

