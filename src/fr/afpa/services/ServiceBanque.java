package fr.afpa.services;

import java.util.Scanner;

import fr.afpa.controle.ControleComptes;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.controle.ControleVerif;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;

import fr.afpa.ihm.AffichageBanque;

import fr.afpa.ihm.AffichageMenu;

public class ServiceBanque {

	/**
	 * Methode pour rechercher une Agence dans la liste des agences de la banque
	 * @param codeAgence pour comparer avec les codes des agences contnues dans la banque
	 * @param banque dans la quelle se trouvent les Agences
	 * @return agence trouvée
	 */
	public Agence rechercherAgence(String codeAgence, Banque banque) {

		for (Agence agence : banque.getListeAgence()) {
			if(codeAgence.equals(agence.getCodeAgence())){
				return agence;
			}

		}
		return null;
	}


	/**
	 * Methode pour rechercher le client dans la banque qui contient les agences,
	 * @param login
	 * @param banque
	 * @return un client si trouvé, un null dans le cas contraire
	 */
	public Client rechercherClient(String login,Banque banque) {
		Client client;
		ServiceAgence agc =new ServiceAgence();
		for (Agence agence : banque.getListeAgence()) {
			client = agc.rechercherClient(login, agence);
			if(client!=null) {
				return client;
			}
		}
		return null;
	}

	/**
	 * Methode pour rechercher un Client via son identifiant
	 * @param identifiant: une chaine de caracteres
	 * @param banque : la banque qui contient le client
	 * @return un Client si la recherche a abouti sinon renvoit un null
	 */

	public Client rechercherIdentifiantClient(String identifiant, Banque banque) {
		ServiceAgence ag= new ServiceAgence();

		Client client; 

		for (Agence agence: banque.getListeAgence()){

			client=ag.rechercherIdentifiantClient(identifiant, agence);
			if(client!=null){
				return client;
			}

		}
		return null;
	}

	/**Methode pour rechercher le Client via son nom
	 * @param nom du Client recherch�
	 * @param banque
	 * @return client si  client trouv� sinon null;
	 */
	public Client rechercherNomClient(String nom, Banque banque) {
		ServiceAgence ag= new ServiceAgence();

		Client client;

		for (Agence agence : banque.getListeAgence()) {

			client=ag.rechercherNomClient(nom, agence);

			if(client!=null) {

				return client;
			}

		}
		return null;
	}

	/**
	 * Methode qui recherche le conseiller dans la liste des agences
	 * @param id via le quel on va chercher le conseiller
	 * @param banque
	 * @return le conseiller si trouv� sinon renvoit un null
	 */
	public Conseiller rechercherConseiller(String id,Banque banque) {
		Conseiller conseiller;
		
		ServiceAgence ag= new ServiceAgence();
		
		for (Agence agence : banque.getListeAgence()) {
			
			conseiller = ag.rechercherConseiller(id, agence);
			
			if(conseiller!=null) {
				
				return conseiller;
			}
		}
		return null;
	}
	
	public Conseiller rechercherConseillerViaLogin(String login, Banque banque) {
		Conseiller conseiller;
		
		ServiceAgence ag= new ServiceAgence();
		
		for (Agence agence : banque.getListeAgence()) {
			
			conseiller = ag.rechercherConseillerViaLogin(login, agence);
			
			if(conseiller!=null) {
				
				return conseiller;
			}
		}
		return null;
	}
	
	/**
	 * Rechercher le client via son numero de compte Bancaire
	 * @param numeroCB 
	 * @param banque dans la quelle se trouve le client
	 * @return le client trouvé
	 */
	public Client rechercherClientViaCompte(String numeroCB, Banque banque) {
		Client clientRecherche;

		ServiceAgence ag= new ServiceAgence();

		for(Agence agence: banque.getListeAgence()) {

			clientRecherche=ag.rechercherClientViaCompte(numeroCB, agence);

			if(clientRecherche!=null) {

				return clientRecherche;
			}

		}
		return null;
	}


	/**
	 * Methode pour desactiver le client via son identifiant
	 * @param banque on prends en compte la banque ou le client est inscrit
	 */
	public void desactiverClient(Banque banque) {
	
		Client client;
		Scanner in=new Scanner(System.in);

		System.out.println("Pour desactiver un client veuillez entrer son numero d'identifiant :");
		String id=ControleSaisie.saisieIdentifiant(in);
		client=rechercherIdentifiantClient(id, banque);

		if(client!=null) {
			client.setStatus(false);
			System.out.println("Ce client a bien ete desactive");
		}
		else {
			System.out.println("Client introuvable ou inexistant");
		}
	}


	/**
	 * Changer la domiciliation d'un client	
	 * @param banque dans la quelle se trouve le client
	 * @return boolean true si le changement � bien �t� effectu�, false dans le cas contraire
	 */
	public boolean changerDomiciliationClient(Banque banque) {
		Client client;
		Client clienTemp;
		Agence agence;
		Conseiller co;

		Scanner in= new Scanner(System.in);

		System.out.println("Veuillez entrer l'identifiant du client");
		String id=ControleSaisie.saisieIdentifiant(in);

		System.out.println("Veuillez entrer le code de l'agence dans la quelle vous souhaitez transferer le client");
		String codeA=ControleSaisie.saisieChiffres(in, 3);

		clienTemp = rechercherIdentifiantClient(id, banque);

		agence=rechercherAgence(codeA,banque);


		if(clienTemp!=null&&agence!=null){

			client=clienTemp;
			for (Conseiller conseiller : agence.getListeConseiller()) {
				System.out.println(conseiller);
			}	

			System.out.println();
			System.out.println("Veuillez entrer l'identifiant du conseiller choisi pour le client.\nIdentifiant: ");
			String idConseiller=in.nextLine();
			co=rechercherConseiller(idConseiller, banque);

			if(co!=null) {

				co.getListeClient().add(client);
				clienTemp.setStatus(false);
				return true;
			}
			else {
				System.out.println("Ce conseiller n'existe pas");
				return false;
			}
		}
		//si le client n'existe pas affiche erreur
		else if(clienTemp==null) {
			System.out.println("Ce client n'existe pas");
			return false;
		}
		//si l'agence n'existe pas
		else if (agence==null) {
			System.out.println("Agence inexistante");
			return false;
		}

		else {
			System.out.println("Erreur dans la saisie veuillez recommencer");

			return false;
		}

	}

	/**
	 * Methode pour rechercher un compte Bancaire via Banque
	 * @param numeroCompte
	 * @param banque
	 * @return CompteCourant recherch�.
	 */
	public Compte rechercherCompteB(String numeroCompte, Banque banque) {
		Compte compte;

		ServiceAgence sag=new ServiceAgence();

		for (Agence agence : banque.getListeAgence()) {
			compte=sag.rechercherCompteB(numeroCompte, agence);
			if(compte!=null&&numeroCompte.equals(compte.getNumeroCompteB())) {
				return compte;

			}

		}
		return null;
	}
	/**
	 * Methode pour ajouter un conseiller à la banque
	 * @param banque
	 * @return un boulean vrai si l'operation s'est bien deroule, sinon faux.
	 */
	public boolean ajouterNouveauConseiller(Banque banque) {
		Scanner in =new Scanner(System.in);
		ServiceConseiller servConseil=new ServiceConseiller();

		Agence agence;

		System.out.println("Dans quelle agence souhaitez vous ajouter un conseiller?");

		String reponseAdmin=ControleSaisie.saisieChiffres(in, 3);

		if(rechercherAgence(reponseAdmin, banque)==null) {
			System.out.println("Cette agence n'existe pas");
			return false;
		}
		else {
			agence=rechercherAgence(reponseAdmin, banque);
			agence.getListeConseiller().add(servConseil.creerConseiller(banque));
			System.out.println("Le conseiller a bien été crée");
			return false;
		}
	}

	/**
	 * Methode pour ajouter le nouveau client dans l'agence et avec le conseiller choisi.
	 * @param banque
	 * @return boolean vrai ou faux selon si l'operation a reussi ou non
	 */
	public boolean ajouterNouveauClient(Banque banque) {
		Scanner in =new Scanner(System.in);
		Agence agence;
		Conseiller conseiller;

		ServiceClient servCl=new ServiceClient();
		
		if(!ControleVerif.siBanquePossedeDesAgences(banque)){
			return false;
		}
		else {
			
			AffichageBanque.afficherToutesAgenceBanque(banque);
			System.out.println("Dans quelle agence voulez vous situer le nouveau client");
			String reponseAgence = ControleSaisie.saisieChiffres(in, 3);
			agence=rechercherAgence(reponseAgence, banque);
			if(!ControleVerif.siAgenceContientConseiller(agence)) {
				return false;
			}
			else {
				System.out.println("Choisissez un Conseiller dans la liste : ");
				System.out.println("\t\t***");
				AffichageBanque.afficherConseillers(agence);
				String reponseChoixConseiller=in.nextLine();
				conseiller=rechercherConseiller(reponseChoixConseiller, banque);
				if(conseiller==null) {
					return false;
				}
				else {
					conseiller.getListeClient().add(servCl.creerClient(banque));
					return true;

				}
			}	
		}
	}
	
	/**
	 * Methode pour verifier s'il y a des conseillers et les afficher.
	 * @param banque
	 * @return
	 */
	public boolean affichageEtVerificationsConseiller(Banque banque) {
		Scanner in=new Scanner(System.in);
		Agence agence;
		System.out.println("Les conseillers de quelle agence souhaitez vous afficher");
		
		String reponseAdmin=ControleSaisie.saisieChiffres(in, 3);
		if(rechercherAgence(reponseAdmin, banque)==null) {
			System.out.println("Cette agence n'existe pas");
			return false;
		}
		else {
			agence=rechercherAgence(reponseAdmin, banque);
			AffichageBanque.afficherConseillers(agence);
			return true;
		}
		}
	

	/**
	 * Methode pour effectuer un virement
	 * @param client pour qui l'utilisateur souhaite faire un virement
	 * @param banque dans la quelle se passe la transaction
	 */
	public boolean effectuerVirement(Client client, Banque banque) {
		Scanner sc = new Scanner(System.in);
		double montant=0;
		Compte compteUn;
		Compte compteDeux;
		int reponse=0;
		String numCBRecherche;
		ServiceCompteB scb=new ServiceCompteB();


		ServiceClient scT=new ServiceClient();

		//AffichageBanque.afficherComptes(client);

		System.out.println("Entrez le numero du compte que vous souhaitez débiter ?");
		String numCompte=ControleSaisie.saisieChiffres(sc, 11);		

		compteUn=scT.rechercherCompteB(numCompte, client);

		if(compteUn!=null&&compteUn.isCompteActif()==true) {
			System.out.println("Entrer le montant");

			montant=ControleComptes.isPositif();

			//verification que le compte soit pas en negatif ici !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

			if((compteUn.getSolde()-montant)<0&&compteUn.isDecouvert()==false) {
				System.out.println("Ce compte ne peut �tre � d�couvert!");
				return false;	
			}
			else {
				while(true) {
					System.out.println("Voulez vous :\n1- Crediter un compte d'une banque externe"
							+ "\n2- Crediter un compte interne à cette banque"
							+ "\n3- Crediter un de vos comptes");

					reponse=Integer.parseInt(ControleSaisie.saisieChiffres(sc, 1));


					if(reponse==1||reponse==2||reponse==3) {
						break;
					}
				}
				switch(reponse) {
				case 1: System.out.println("Entrez le numero de compte banquaire externe : ");
				numCBRecherche=ControleSaisie.saisieChiffres(sc, 11);
				scb.retraitCompte(compteUn, montant);
				return true;


				case 2: System.out.println("Entrez le numero de compte banquaire : ");
				numCBRecherche=ControleSaisie.saisieChiffres(sc, 11);
				compteDeux=rechercherCompteB(numCBRecherche, banque);
				if(compteDeux!=null&&compteDeux.isCompteActif()==true) {
					scb.retraitCompte(compteUn, montant);
					scb.alimenterCompte(compteDeux, montant);
					return true;
				}
				else {
					System.out.println("Ce numero de compte n'existe pas ou le compte est desactiv�");
					return false;
				}

				case 3: System.out.println("Entrez le numero de compte banquaire : ");
				numCBRecherche=ControleSaisie.saisieChiffres(sc, 11);
				compteDeux=scT.rechercherCompteB(numCBRecherche, client);

				if(compteDeux!=null&&compteDeux.isCompteActif()==true) {
					scb.retraitCompte(compteUn, montant);
					scb.alimenterCompte(compteDeux,montant);
					return true;
				}
				else {
					System.out.println("Ce numero de compte n'existe pas ou le compte est desactiv�");
					return false;

				}
				}
			}


		}

		return false;
	}


	public void choixAdmin(Banque banque) {
		ServiceAgence servAgence = new ServiceAgence();
		ServiceBanque servBank = new ServiceBanque();
		ServiceConseiller servCons=new ServiceConseiller();
		Scanner scanner = new Scanner(System.in);
		
		boolean quitter = false;
	
		String choix = "";
	
		while (!quitter) {
	
			AffichageMenu.menuApresConnexionAdmin();
			choix = scanner.nextLine();
			if(choix.length()>0) {
			switch (choix.charAt(0)) {
	
			case '1':
				banque.getListeAgence().add(servAgence.creerAgence(banque));
				System.out.println("L'agence à bien été créee");
				break;
	
			case '2':if(!ControleVerif.siBanquePossedeDesAgences(banque)) {
				break;
			}
			else {
				AffichageBanque.afficherToutesAgenceBanque(banque);
				break;
			}
	
			case '3': 
				if(!ControleVerif.siBanquePossedeDesAgences(banque)) {
					break;
				}
				else {
					AffichageBanque.afficherToutesAgenceBanque(banque);
					servBank.ajouterNouveauConseiller(banque);
					break;
				}
					
			case '4': 
				servBank.affichageEtVerificationsConseiller(banque);
				break;
			case '5':
				servCons.gestionClientParConseiller(banque, null);
				break;
				
			case '6': 
				if(ControleVerif.siBanquePossedeDesAgences(banque)){
				desactiverClient(banque);
				
				break;
				}
			case '7':
			case'8': quitter=true;
			break;
			}
			}
		}
	}
}
