package fr.afpa.services;


import java.util.Scanner;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.controle.ControleVerif;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.AffichageClient;
import fr.afpa.ihm.AffichageMenu;

public class ServiceConseiller {
	
	
		public Conseiller creerConseiller(Banque banque) {
			Scanner in= new Scanner(System.in);
			String login="";

			System.out.println("Entrez le nom du Conseiller");
			String nom=ControleSaisie.saisieLettre(in);


			System.out.println("Entrez le prenom du Conseiller");
			String prenom=ControleSaisie.saisieLettre(in);

			System.out.println("Entrez un mail valide");
			String mail=ControleSaisie.saisieEmail(in);
			
			 do{
			
			System.out.println("Entrez le login specifique au conseiller");
			login = ControleSaisie.siLoginConseillerCorrect(in);
			 }while(!ControleVerif.siLoginConseillerExisteDeja(login, banque));
			
			System.out.println("Veuillez saisir un mot de passe pour la connexion du conseiller:");
			String mdp=in.nextLine();
			
			Conseiller conseiller =new Conseiller(nom, prenom, login, mail, mdp);
			
			System.out.println("L'identifiant du conseiller est : "+conseiller.getIdentifiantConseiller());
			return conseiller;
			
			
		}
		
		/**
		 * Methode pour rechercher le client dans la liste des clients du conseiller via login
		 * @param login du client
		 * @param conseiller qui contient le client
		 * @return un client, si pas trouv� retourne un null;
		 */
		public Client rechercherClient(String login, Conseiller conseiller) {

			for (Client client : conseiller.getListeClient()) {
				if(login.equals(client.getLogin())){
					return client;
				}
			}
			return null;
		}
		/**
		 * Methode pour chercher le client dans la liste des conseiller via son nom
		 * @param nom
		 * @param conseiller
		 * @return
		 */
		public Client rechercherNomClient(String nom, Conseiller conseiller) {

			Client clientTrouve=null;

			int occurence=0;

			for (Client client : conseiller.getListeClient()) {
				if(nom.equals(client.getNom())){
					occurence++;
					clientTrouve=client;
				}
			}

			if (occurence==1&&clientTrouve!=null) {
				return clientTrouve;
			}
			else {
				System.out.println("Client introuvable, veuillez essayer de chercher via l'identifiant du client");
			}
			return null;
		}	
		/**
		 * Rechercher le client via son numero de compte Bancaire
		 * @param numeroCB 
		 * @param conseiller
		 * @return le client trouv�
		 */
		public Client rechercherClientViaCompte(String numeroCB, Conseiller conseiller) {
			ServiceClient cl=new ServiceClient();
			
			Client clientRecherche;
			for(Client client: conseiller.getListeClient()) {
				clientRecherche=cl.rechercherClientViaCompte(numeroCB, client);
				if(clientRecherche!=null) {
					return clientRecherche;
				}

			}
			return null;
		}

		/**
		 * Methode pour rechercher le Client via son identifiant
		 * @param identifiant du client qu'on recherche
		 * @param conseiller 
		 * @return un client si trouvé, dans le cas contraire revnoit un null;
		 */
		public Client rechercherIdentifiantClient(String identifiant, Conseiller conseiller) {

			for (Client client : conseiller.getListeClient()){
				if(identifiant.equals(client.getId())){
					return client;
				}

			}
			return null;
		}
		
		/**
		 * Methode pour rechercher un compte Bancaire via Conseiller
		 * @param numeroCompte
		 * @param conseiller
		 * @return CompteCourant recherch�.
		 */
		public Compte rechercherCompteB(String numeroCompte, Conseiller conseiller) {
			Compte compte;
			
			ServiceClient scl=new ServiceClient();
			
			for (Client client : conseiller.getListeClient()) {
				compte=scl.rechercherCompteB(numeroCompte, client);
				if(compte!=null&&numeroCompte.equals(compte.getNumeroCompteB())) {
					return compte;

				}

			}
			return null;
		}
		/**
		 * Methode gestion client
		 * @param banque
		 * @param conseiller
		 */
		public void gestionClientParConseiller(Banque banque, Conseiller conseiller) {
		ServiceBanque servBank=new ServiceBanque();
		ServiceCompteB servCompB=new ServiceCompteB();
		Client cl=null;
		Compte cmpt=null;
		Scanner scanner=new Scanner(System.in);
		ServiceClient servClient=new ServiceClient();
		boolean quitter=false;
		String choix="";
		String repUtilis="";
		
		while(!quitter) {
			AffichageMenu.menuChoixGestionClient();
			choix=scanner.nextLine();
			
			switch(choix.charAt(0)) {
			
			case'0': servBank.ajouterNouveauClient(banque);
			
			break;
			
			case'1': 
				
				AffichageClient.afficherInfosClient(banque);
					break;
			case'2':System.out.println("Entrez l'identifiant du client dont vous souhaitez modifier les informations"); 
				repUtilis=ControleSaisie.saisieIdentifiant(scanner);
				cl=servBank.rechercherIdentifiantClient(repUtilis, banque);
				if(cl!=null) {
				System.out.println();	
				servClient.modificationClient(cl);
				break;
				}
				else {
				System.out.println("Ce client n'existe pas");
					break;
					}
			case '3': System.out.println("Quel compte souhaitez vous afficher?");
				repUtilis=scanner.nextLine();
				cmpt=servBank.rechercherCompteB(repUtilis, banque);
				if(cmpt!=null) {
					System.out.println(cmpt);
				
				break;
				}
				else {
					System.out.println("Ce compte n'existe pas");
					break;
				}
			case '4':
				System.out.println("Acces aux comptes, entrez l'identifiant du client");
				repUtilis=scanner.nextLine();
				cl=servBank.rechercherIdentifiantClient(repUtilis, banque);
				if(cl!=null) {
					servClient.gestionComptes(cl);
				break;
				}
					
				else {
				System.out.println("Client introuvable ou inexistant");
				break;
				}
				
			case'5':quitter=true; break;
			default:System.out.println("Commande invalide");
				
		}
		}	
		}

}
