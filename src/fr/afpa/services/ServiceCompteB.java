package fr.afpa.services;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.Scanner;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.property.TextAlignment;

import fr.afpa.controle.ControleSaisie;
import fr.afpa.controle.ControleVerif;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PlanEpargneLogement;
import fr.afpa.ihm.AffichageBanque;
import fr.afpa.ihm.AffichageMenu;

public class ServiceCompteB {

	public void creerFichierInfo() {

	}

	/**
	 * Methode pour crï¿½er un compte bancaire soit un compte courant, soit un
	 * livret A, soit PEL.
	 */
	public Compte creerCompteBancaire(Client client) {
		Compte compte;

		boolean flag = false;
		boolean decouvert = false;
		int rep;
		int type = 0;
		Scanner sc = new Scanner(System.in);

		boolean choixValide = false;
		while (!choixValide) {
			System.out.println("Quel type de compte souhaitez vous ouvrir?" + "\n1- Compte Courant" + "\n2- Livret A"
					+ "\n3- Plan epargne logement");

			type = sc.nextInt();
			if (type == 1 || type == 2 || type == 3) {
				choixValide = true;
			}

		}

		String numCompte = ControleVerif.GenererNumeroAleatoire(11);
		System.out.println("Le numero de compte bancaire de cet utilisateur est : " + numCompte);

		System.out.println("Somme à deposer sur le compte :");
		double solde = sc.nextDouble();

		System.out.println("Le client dispose t-il d'un dï¿½couvert autorisï¿½ pour ce compte?");

		while (!flag) {
			System.out.println("Veuillez choisir entre 1- Oui 2- Non");
			rep = sc.nextInt();
			sc.nextLine();
			if (rep == 1) {
				decouvert = true;
				flag = true;
			} else if (rep == 2) {
				flag = true;
			} else {
				System.out.println("Commande invalide");
			}
		}

		boolean compteActif = true;
		switch (type) {

		case 1:
			Compte compte_client = new CompteCourant(numCompte, solde, decouvert, compteActif);
			calculerFrais(compte_client);
			return compte_client;
			
		case 2:
			Compte livretA_client = new LivretA(numCompte, solde, decouvert, compteActif);
			livretA_client.setFrais(calculerFrais(livretA_client));
			return livretA_client;
		
		case 3:
			Compte PEL_client =new PlanEpargneLogement(numCompte, solde, decouvert, compteActif);
			PEL_client.setFrais(calculerFrais(PEL_client));
			return PEL_client;
		default: System.out.println("Erreur Saisie");
		break;
		}
		return null;
			
	}


//	private void releveDeCompte(Client client, Compte compte, LocalDate dateDonne) throws FileNotFoundException {
//
//		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("_dd_MM_yyyy_HH_mm_ss");
//		LocalDateTime now = LocalDateTime.now();
//
//		String sortie = "C:\\ENV\\Workspace\\projet_banque_v2\\src\\fr\\afpa\\Sortie\\Releve_de_compte\\Releve_de_compte_de_"
//				+ client.getNom() + "_" + client.getPrenom() + dtf.format(now) + ".pdf";
//		PdfWriter writer = new PdfWriter(sortie);
//		PdfDocument pdf = new PdfDocument(writer);
//
//		FileReader fr = new FileReader(
//				"C:\\ENV\\Workspace\\projet_banque_v2\\src\\fr\\afpa\\Sortie\\Historique\\historique_ "
//						+ client.getPrenom() + "_" + client.getNom() + ".txt");
//
//		BufferedReader br = new BufferedReader(fr);
//
//		Document document = new Document(pdf, PageSize.A4);
//
//		dtf = DateTimeFormatter.ofPattern("dd_MM_yyyy");
//
//		Paragraph p = new Paragraph().setTextAlignment(TextAlignment.CENTER);
//		p.add("-----------------La banque � Picsou-----------------\n");
//		p.add(client.getNom() + " " + client.getPrenom() + " " + "\n");
//		p.add("Numero de compte courant : " + CompteCourant.getNumeroCompteB() + "\n");
//		p.add("A la date du " + dtf.format(now) + "votre solde du compte courant est de " + compte.getSolde + " \n");
//		p.add("Numero de compte livret A : " + compte. + "\n");
//		p.add("A la date du " + dtf.format(now) + "votre solde du compte livret A est de " + LivretA.getSolde()
//				+ " \n");
//		p.add("Numero de compte plan epargne logement : " + PlanEpargneLogement.getNumeroCompteB() + "\n");
//		p.add("A la date du " + dtf.format(now) + "votre solde du compte plan d'epargne logement est de "
//				+ PlanEpargneLogement.getSolde() + " \n");
//		// p.add((historique du compte banquaire);
//		document.add(p);
//		document.close();
//	}

	private void historiqueDesTransactions(Client client, Compte compte, char operation, double montant,
			double nveauSolde) throws IOException {

		String destination = "C:\\ENV\\Workspace\\projet_banque_v2\\Ressources\\Clients\\transactions_"
				+ client.getPrenom() + "_" + client.getNom() + ".csv";

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("_dd_MM_yyyy");
		LocalDateTime now = LocalDateTime.now();
		FileWriter fw = new FileWriter(destination);
		BufferedWriter bw = new BufferedWriter(fw);

		if (compte instanceof CompteCourant) {
			bw.write("CC" + ";" + dtf + ";" + operation + ";" + montant);
		}
		if (compte instanceof LivretA) {
			bw.write("LA" + ";" + dtf + ";" + operation + ";" + montant);
		}
		if (compte instanceof PlanEpargneLogement) {
			bw.write("PEL" + ";" + dtf + ";" + operation + ";" + montant);
			bw.newLine();
			bw.close();
		}
	}

	public void lecteurCsv(Client client, String dateIn, String dateOut) {
		String file = "C:\\ENV\\Workspace\\projet_banque_v2\\Ressources\\Clients\\Transactions_" + client.getPrenom()
				+ "_" + client.getNom() + ".csv";

		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()) {
				String lecteur = br.readLine();
				String[] historique = lecteur.split(";");
				LocalDate date1 = LocalDate.parse(dateIn);
				LocalDate date2 = LocalDate.parse(dateOut);
				LocalDate date3 = LocalDate.parse(historique[2]);
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("_dd_MM_yyyy");

				while (br.readLine() != null) {
					br.readLine();

					if (date1.isBefore(date3)&&date2.isAfter(date3)) {
						
							System.out.println(historique);
						}
					
				}

			}
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Methode pour deposer un montant sur un compte
	 * 
	 * @param compte  sur le quel le client souhate ajouter le montant donnï¿½e
	 * @param montant donnï¿½ par l'utilisateur.
	 */
	public void alimenterCompte(Compte compte, double montant) {
		compte.setSolde(compte.getSolde() + montant);
	}

	/**
	 * Methode pour retirer la somme entrée en parametres
	 * 
	 * @param compte  compte que le client souhaite debiter
	 * @param montant le montant que l'utilisateur entre
	 */
	public void retraitCompte(Compte compte, double montant) {

		if (compte.getSolde() - montant < 0 && compte.isDecouvert() == false) {
			System.out.println("Retrait impossible, solde insuffisant");
		} else {
			compte.setSolde(compte.getSolde() - montant);
		}

	}

	/**
	 * Calcul des frais en fonction du profil du compte
	 * 
	 * @param compte
	 * @return
	 */
	public double calculerFrais(Compte compte) {
		if (compte instanceof CompteCourant) {
			return compte.getFrais();
		} else if (compte instanceof LivretA) {
			return compte.getFrais() + (0.1 * compte.getSolde());
		} else if (compte instanceof PlanEpargneLogement) {
			return compte.getFrais() + (0.025 * compte.getSolde());
		}
		return compte.getFrais();
	}

	
}
