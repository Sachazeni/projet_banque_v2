package fr.afpa.services;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

import fr.afpa.controle.ControleComptes;
import fr.afpa.controle.ControleSaisie;
import fr.afpa.controle.ControleVerif;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.ihm.AffichageComptes;
import fr.afpa.ihm.AffichageMenu;

public class ServiceClient {



	/**
	 * Methode pour créer un client,
	 * @param banque Prend la banque dans la quelle le client sera cr�e
	 * @return
	 */
	public Client creerClient(Banque banque) {
		Scanner in= new Scanner(System.in);


		System.out.println("Entrez le nom du Client");
		String nom=ControleSaisie.saisieLettre(in);


		System.out.println("Entrez le prenom du Client");
		String prenom=ControleSaisie.saisieLettre(in);


		System.out.println("Entrez la date de naissance du Client");
		LocalDate date=ControleSaisie.saisieDate(in);


		System.out.println("Entrez un mail valide");
		String mail=ControleSaisie.saisieEmail(in);

		String login = generationLoginClient(banque);
		System.out.println("Le login du client est : "+login);

		System.out.println("Veuillez saisir un mot de passe pour la connexion du client:");
		String mdp=in.nextLine();

		Client client=new Client(nom, prenom, date, login, mail, mdp);
		client.setStatus(true);

		System.out.println("L'identifiant du client est : "+client.getId());
		return client;

	}


	/**
	 * Methode pour rechercher un compte Bancaire dans la liste des comptes du client
	 * @param numeroCompte
	 * @param client
	 * @return CompteCourant recherch�.
	 */
	public Compte rechercherCompteB(String numeroCompte, Client client) {

		for (Compte compte : client.getListeCompte()) {

			if(numeroCompte.equals(compte.getNumeroCompteB())&&compte!=null) {

				return compte;

			}

		}
		return null;
	}

	/**
	 * M�thode pour generer un login al�atoire de 10 chiffres pour le client
	 * @return String une chaine de caracteres
	 */
	public String generationLoginClient(Banque banque) {

		String login="";
		Random chiffreAlea=new Random();
		do {
			for (int i = 0; i < 10; i++) {
				login=login+chiffreAlea.nextInt(10);	
			}
		}
		while(!ControleVerif.siLoginClientDisponible(login, banque));

		return login;
	}

	/**
	 *Methode pour modifier les informations d'un client
	 * @param choix - un entier qui designe le choix de l'utilisateur d'apres le menu
	 * @param client sur le quel l'utilisateur souhaite effectuer des modifications
	 */
	public void modificationClient(Client client) {

		Scanner in=new Scanner(System.in);
		String choix="";
		boolean quitter=false;

		while(!quitter) {

			AffichageMenu.menuModifClient();

			choix=in.nextLine();
			if(choix.length()>0) {
				switch (choix.charAt(0)) {
				case '1': System.out.println("Entrez le nouveau nom du client :");
				String nom=ControleSaisie.saisieLettre(in);
				client.setNom(nom);
				System.out.println("Le Profil à bien été modifiée: "+client);
				break;

				case '2': System.out.println("Entrer le nouveau pr�nom");
				String prenom=ControleSaisie.saisieLettre(in);
				client.setPrenom(prenom);
				System.out.println("Le Profil à bien été modifiée: "+client);
				break;

				case '3': System.out.println("Entrer la nouvelle adresse mail");
				String mail=ControleSaisie.saisieEmail(in);
				client.setEmail(mail);
				System.out.println("Le Profil client à bien été modifiée: "+client);
				break;

				case '4': System.out.println("Entrer la date de naissance");
				LocalDate nouvDate=ControleSaisie.saisieDate(in);
				client.setDateNaissance(nouvDate);
				System.out.println("Le profil client à bien été modifiée: "+client);
				break;
				case '5': quitter=true;
				break;

				default: System.out.println("Commande invalide");
				break;

				}
			}
		}
	}

	/** 
	 * Methode de recherche de client via son numero de compte Bancaire
	 * @param numCompte
	 * @param client
	 * @return
	 */
	public Client rechercherClientViaCompte(String numCompte, Client client) {

		for (Compte compte : client.getListeCompte()) {
			if(numCompte.equals(compte.getNumeroCompteB())) {

				return client;

			}
		}
		return null;
	}
	/**
	 * Methode pour ajouter un nouveau compte dans le client
	 * @param banque
	 * @return boolean vrai ou faux selon si l'operation a reussi ou non
	 */
	public Client verifAvantAjoutNouveauCompte(Banque banque) {
		Client client;
		Scanner in =new Scanner(System.in);
		ServiceBanque servBanq=new ServiceBanque();


		System.out.println("Pour quel Client Voulez vous ouvrir un compte? Rentrez son identifiant");
		String reponseUtilisateur=ControleSaisie.saisieIdentifiant(in);
		client=servBanq.rechercherIdentifiantClient(reponseUtilisateur, banque);
		if(client!=null) {
			return client;
		}
		else {
			return null;
		}

	}
	/**
	 * methode de gestion du compte bancaire
	 * @param client
	 */
	public void gestionComptes(Client client){
		boolean quitter=false;
		ServiceClient servCl=new ServiceClient();
		Compte compte;
		ServiceCompteB servCb=new ServiceCompteB();
		Scanner in = new Scanner(System.in);
		String choix="";
		String choixcompte="";
		Double montant=0d;
		while(!quitter) {

			AffichageMenu.menuGestionComptes();
			choix=in.nextLine();
			if(choix.length()>0) {
				switch(choix.charAt(0)) {
				case '0': 
					if(ControleComptes.siPossedeTroisComptes(client)) {
						break;
					}
					else {
						client.getListeCompte().add(servCb.creerCompteBancaire(client));
						AffichageComptes.afficherComptes(client);
					}
				case '1': break;
				case'2': break;
				case '3':

					AffichageComptes.afficherComptes(client);
					if(client.getListeCompte().size()==0) {
						System.out.println("Ce client n'as pas encore de comptes");
						break;
					}
					System.out.println("De quel compte souhaitez vous retirer ?");
					choixcompte=ControleSaisie.saisieChiffres(in, 11);

					compte=servCl.rechercherCompteB(choixcompte, client);
					System.out.println("Indiquez le montant");
					montant=in.nextDouble();

					if(compte!=null) {
						servCb.retraitCompte(compte, montant);
						AffichageComptes.afficherComptes(client);
						break;
					}
					else {
						System.out.println("Ce compte n'existe pas");
						break;
					}
				case '4': 

					if(client.getListeCompte().size()==0) {
						System.out.println("Ce client n'as pas encore de comptes");
						break;
					}
					System.out.println("Quel compte souhaitez vous deposer l'argent ?");
					choixcompte=ControleSaisie.saisieChiffres(in, 11);

					compte=servCl.rechercherCompteB(choixcompte, client);
					System.out.println("Indiquez le montant");
					montant=in.nextDouble();

					if(compte!=null) {
						servCb.alimenterCompte(compte, montant);
						AffichageComptes.afficherComptes(client);
						break;
					}
					else {
						System.out.println("Ce compte n'existe pas");
						break;
					}

				case '5': break;


				}

			}
		}



	}


	public void menuSpecialClient(Client client){
		boolean quitter=false;
		ServiceClient servCl=new ServiceClient();
		Compte compte;
		ServiceCompteB servCb=new ServiceCompteB();
		Scanner in = new Scanner(System.in);
		String choix="";
		String choixcompte="";
		Double montant=0d;
		while(!quitter) {

			AffichageMenu.menuSpecialClient();
			choix=in.nextLine();
			if(choix.length()>0) {
				switch(choix.charAt(0)) {
				case '1': System.out.println(client);
				break;
				case'2':

					if(client.getListeCompte().size()==0) {
						System.out.println("Vous n'avez pas encore de comptes");
						break;
					}
					AffichageComptes.afficherComptes(client);
					break;



				case '3': if(client.getListeCompte().size()==0) {
					System.out.println("Vous n'aves pas encore de comptes");
					break;
				}
				System.out.println("Dans quel compte souhaitez vous deposer l'argent ?");
				choixcompte=ControleSaisie.saisieChiffres(in, 11);

				compte=servCl.rechercherCompteB(choixcompte, client);
				System.out.println("Indiquez le montant");
				montant=in.nextDouble();

				if(compte!=null) {
					servCb.alimenterCompte(compte, montant);
					AffichageComptes.afficherComptes(client);
					break;
				}
				else {
					System.out.println("Ce compte n'existe pas");
					break;
				}

				case '4':

					if(client.getListeCompte().size()==0) {
						System.out.println("Ce client n'as pas encore de comptes");
						break;
					}
					AffichageComptes.afficherComptes(client);
					System.out.println("De quel compte souhaitez vous retirer ?");
					choixcompte=ControleSaisie.saisieChiffres(in, 11);

					compte=servCl.rechercherCompteB(choixcompte, client);
					System.out.println("Indiquez le montant");
					montant=in.nextDouble();

					if(compte!=null) {
						servCb.retraitCompte(compte, montant);
						AffichageComptes.afficherComptes(client);
						break;
					}
					else {
						System.out.println("Ce compte n'existe pas");
						break;
					}
				case '5': System.out.println("Operation indisponible pour le moment, Veuillez nous excuser pour la gene occasionnée");

				break;

				case '6': quitter=true;break;
				}
			}


		}
	}
}

